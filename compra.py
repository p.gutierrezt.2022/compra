#!/usr/bin/env pythoh3

'''
Programa para mostrar la lista de la compra
'''

habitual = ("patatas","leche","pan")

def main():

    especifica = []
    while True:

        A=input("Elemento a comprar: ")

        if A != "":
            especifica.append(A)

        if not A:
            completa = []
            for B in habitual:
                completa.append(B)
            for C in especifica:
                if C not in completa:
                    completa.append(C)
            break

    lista_total = set(completa)
    print("Lista de la compra:")

    for elemento in completa:
            print(elemento)

    print(f"Elementos habituales: {len(habitual)}")
    print(f"Elementos específicos: {len(especifica)}")
    print(f"Elementos en lista: {len(lista_total)}")



if __name__ == '__main__':
    main()


